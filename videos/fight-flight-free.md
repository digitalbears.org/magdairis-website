---
title: The Making of Fight, Flight, Free
vimeo-id: 233215955
thumbnail-id: 654488199
date: 2017-11-01
---

Dutch art director, [**Iris Jacobs**](http://www.irisjacobs.nl/), connects a multidisciplinary team of Barcelona's international talent to create her surrealist debut short film. Using community spaces and reclaimed props and costumes, the making of _Fight, Flight, Free_ illustrates the spirit of local creatives determined to support low-to-no budget film making.

_Language: English_