I make visual content that supports social change, environmental action, independent or community projects and accessible learning. Under my umbrella of ‘visual content’ is [**video**](/videos.html), [**photography**](/photos.html) and [**design**](https://www.gitlab.com/digitalbears.org/magdairis-website). 

After recognising that film can be a tool for change makers and a learning process for all involved, I was drawn to documentary filmmaking during my degree and continued studying with Otoxo Productions in Barcelona. Producing several award-nominated shorts propelled my skills and understanding as a filmmaker, cementing my motivation to create responsible and socially conscious films. 

Barcelona is a brilliant example of the intersection between art and social diversity. With an abundance of community projects, it became the city which I evolved in as a freelancer. I produced videos for a diverse range of projects including independent films, nonprofits and education centres. 

Eager to focus entirely on video production, I started full-time as the Visual Editor for one of Europe’s leading wellness startups. There I created, curated and managed the distribution of the visual content. The multifaceted startup environment encouraged me to broaden my skill set; progressing as a photographer, delving into digital marketing and launching my first steps into design.

I have now returned to working freelance, this time with two new approaches - working while travelling and using only open source software. I am especially excited by anything that explores the connection between humans, nature and technology. 

Do you have a project or idea and need a freelancer? Let’s chat :-)  

If you’re a non-profit organisation or individual with little to no-budget, I am open to working in exchange of time, skills, attributions, voluntary donations, food, karma, etc. 
