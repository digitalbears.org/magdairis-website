---
cover-photo: lollipop-ladies
---

It is often the human expression that brings a photo to life. Photographer and viewer alike, we are drawn to the emotional connection between ourselves and the subject, it is what makes photographing people so compelling. Most recently, my focus has shifted towards issues of privacy and permission as a photographer in the digital age. This set of photographs is the result of my exploration with offline/online public spaces and selfie culture. 