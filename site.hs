--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import           Data.Monoid (mappend)
import           Hakyll
import           Control.Applicative (empty)
import           Control.Monad (mplus)
import           Control.Arrow ((>>>))
import           Hakyll.Images (loadImage, ensureFitCompiler)
import           System.FilePath.Posix
import           Hakyll.Core.Util.String (trim)
import           Data.Maybe (fromMaybe)



--------------------------------------------------------------------------------
main :: IO ()
main = hakyll $ do
    match ( "assets/vector/*" .||. "assets/video-thumbs/*" .||. "assets/photo-tag-titles/*" ) $ do
        route idRoute
        compile copyFileCompiler

    match "assets/css/*" $ do
        route   idRoute
        compile compressCssCompiler

    match "templates/**" $ compile templateBodyCompiler

    match "pages/index.md" $ compile pandocCompiler

    let pages = fromList ["pages/index.html", "pages/contact.html"]

    match pages $ do
        route $ gsubRoute "pages/" (const "")
        compile $ do
            indexBlob <- load "pages/index.md"
            let ctx = constField "indexBlob" (itemBody indexBlob) <> defaultContext
            getResourceBody
                >>= applyAsTemplate ctx
                >>= loadAndApplyTemplate "templates/default.html" defaultContext
    
    match "pages/photos.html" $ do
        route $ gsubRoute "pages/" (const "")
        compile $ do
            tagCoverPhotos <- loadAll ("photos/tags/*.md" .&&. hasVersion "cover")
            let photosCtx =
                    listField "tagCoverPhotos" defaultContext (return tagCoverPhotos) `mappend`
                    defaultContext
            getResourceBody
                >>= applyAsTemplate photosCtx
                >>= loadAndApplyTemplate "templates/default.html" defaultContext

    match "photos/*.jpg" $ do
        route idRoute
        compile copyFileCompiler

    match "photos/*.jpg" $ version "480p" $ do
        route $ gsubRoute ".jpg" (const "-480p.jpg")
        compile $ loadImage
            >>= ensureFitCompiler 1000 480

    match "photos/*.md" $ do
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/photos/photo-default.html" photoCtx

    match "photos/tags/*.md" $ version "cover" $ do
        let ctx = photosTitleSvgCtx <> defaultContext
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/photos/cover.html" ctx

    match "photos/tags/*.md" $ version "title" $ do
        let ctx = photosTitleSvgCtx <> defaultContext
        compile $ pandocCompiler
            >>= applyAsTemplate ctx

    tags <- buildTagsWith getTags' "photos/*.md" (fromCapture "photos/*.html")

    tagsRules tags $ \tag pattern -> do
        route idRoute
        compile $ do
            titleBlob <- load $ setVersion (Just "title") $ fromCapture ("photos/tags/*.md") tag
            let ctx = constField "titleBlob" (itemBody titleBlob)
            photos <- loadAll pattern
            let ctx' = listField "photos" photoCtx (return photos)
                        <> photosTitleSvgCtx <> ctx <> defaultContext
            makeItem ""
                >>= loadAndApplyTemplate "templates/photos/for-photos-body.html" ctx'
                >>= loadAndApplyTemplate "templates/default.html" defaultContext

    match "videos/*.md" $ do
        route $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/videos/video-default.html" videoCtx

    match "pages/videos.html" $ do
        route $ gsubRoute "pages/" (const "")
        compile $ do
            videos <- loadAll "videos/*.md" >>= recentFirst
            let videosCtx =
                    listField "videos" videoCtx (return videos) `mappend`
                    defaultContext
            getResourceBody
                >>= applyAsTemplate videosCtx
                >>= loadAndApplyTemplate "templates/default.html" defaultContext

photosTitleSvgCtx :: Context String
photosTitleSvgCtx = withPathField "titleSvg" $
                            takeBaseName
                        >>> dropExtension
                        >>> (++) "templates/svg/photos/"
                        >>> flip (++) "-cover-title.svg"

videoCtx :: Context String
videoCtx = existsBodyField
                <> withPathField "basename" (takeBaseName >>> dropExtension)
                <> defaultContext

photoCtx :: Context String
photoCtx = existsBodyField
                <> withPathField "src" (dropExtension >>> (flip (++) ".jpg"))
                <> withPathField "src-480p" (dropExtension >>> (flip (++) "-480p.jpg"))
                <> defaultContext

withPathField :: String -> (String -> String) -> Context String
withPathField name f = field name $ \item ->
    return $ itemIdentifier >>> toFilePath >>> f $ item

existsBodyField :: Context String
existsBodyField = boolField "existsBody" $ \item ->
    case (trim . itemBody) item of
        "" -> False
        x -> True

getTags' :: MonadMetadata m => Identifier -> m [String]
getTags' identifier = do
    metadata <- getMetadata identifier
    return $ fromMaybe [] $
        (lookupStringList "tags" metadata) `mplus`
        (filter wanted . map (wsToHyphen . trim) . splitAll "," <$> lookupString "tags" metadata)
    where
        wsToHyphen = replaceAll " " (\_ -> "-")
        wanted tag = tag `elem` ["travel", "cities", "nature", "people"]
        -- todo: sort this out...
