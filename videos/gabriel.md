---
title: Gabriel
youtube-id: bs8PpTm_AYs
date: 2016-07-31 
---

Classically trained in Chilean music schools and politically active during the Perón era, Gabriel Brncic was exiled in 1974 to Barcelona where he joined the Phonos Lab. _Gabriel_ uses archive footage to illustrate the emotive account of Brncic's life, from the harrowing distress of extradition to the peace he found in Barcelona. The film is accompanied by Gabriel's own composition.

_Language: Spanish (English subtitles)_