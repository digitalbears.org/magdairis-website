---
title: Isaac
vimeo-id: 236158121
thumbnail-id: 658175042
date: 2017-01-01
---

Located in a northern industrial zone of Barcelona, the Bon Pastor neighbourhood is largely  populated by Roma, and the community is steeped in Romani traditions. Working with director Albert Bougleux and [**The Missing City Stars**](https://www.themissingcitystars.com/), one Bon Pastor teenager questions the practice of Roma girls dropping out of school by age 13.

_Language: Spanish_