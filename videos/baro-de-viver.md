---
title: The Making of Baro de Viver
vimeo-id: 218116691
thumbnail-id: 635506160
date: 2017-03-01
---

 [**The Missing City Stars**](https://www.themissingcitystars.com/) is a small group of documentary film makers providing workshops for minority groups in Barcelona. With the support of a professional film crew to shoot their own documentary, they highlight the different cultures living together in the local community. 

This film looks behind the scenes of the project, led by director Oriol Porta, in Baro de Viver; an isolated neighbourhood in the Sant Andreu district of Barcelona.

_Language: English and Spanish_
