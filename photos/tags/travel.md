---
cover-photo: blue-sea
---

Travel photography is an intersection of the themes present in nature, cities and people, connected by the experience of encountering a new place for the first time. When travelling, themes of identity and authenticity are particularly prevalent in my understanding of cultures and their context. Through photography, I aim to present the complexities and nuances of cultural identities through the diversity of landscapes, people, symbols and customs. 