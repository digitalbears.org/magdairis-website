---
cover-photo: basketball
---

In exploration of how people interact with their surroundings, these photographs document moments of loneliness between the streets of big cities. In adjacency, they capture displays of togetherness despite political or social tension. People carving out their own space, whether it’s for a fleeting embrace with a loved one or to provide shelter for their family.