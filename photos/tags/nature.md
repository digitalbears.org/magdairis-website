---
cover-photo: desert
---

Exploring patterns and formations in landscapes, flora and fauna has greater resonance when cast under the shadow of climate change. Initially, photographing nature was alluring for its stillness. Where cities and people are often transient moments, mountains and deserts provide a reassuring constancy. However, chronic damage to the planet adds an acute sense of urgency to document the environment as it changes.