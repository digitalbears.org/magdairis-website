---
title: Clio and Claudia
youtube-id: VQZ08XDXFTI
date: 2016-07-30
---

When Clio and Claudia started the project, Horta de Belloch was a derelict farmhouse in the Catalonian countryside. After months of hard work implementing the principals of permaculture, Clio and Claudia transformed the abandoned land into a working farm. They now produce enough vegetables, eggs and bread to feed themselves and to sell locally.

_Language: English_